package entities;

/**
 *
 * @author andreacbravob
 */
public class SMOLD {
    int id;
    int label;
    String text;

    public SMOLD() {
    }
    
    public SMOLD(int label, String text) {
        this.label = label;
        this.text = text;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
