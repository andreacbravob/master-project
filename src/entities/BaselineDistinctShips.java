/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author andreacbravob
 */
public class BaselineDistinctShips {
    
    private String shipName;
    private Integer count;
    private String monsterrollen;
    private String ships;
    private String crew;
    private Integer bottomYear;
    private Integer topYear;
    private String shipType;
    private String shipSizes;
    private String captainLastName;
    private String captainFirstNames;
    private Integer id;
    private String requestUrl;
    private String resultXml;
    private Integer numberOfResults;

    public BaselineDistinctShips() {
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getMonsterrollen() {
        return monsterrollen;
    }

    public void setMonsterrollen(String monsterrollen) {
        this.monsterrollen = monsterrollen;
    }

    public String getShips() {
        return ships;
    }

    public void setShips(String ships) {
        this.ships = ships;
    }

    public String getCrew() {
        return crew;
    }

    public void setCrew(String crew) {
        this.crew = crew;
    }

    public Integer getBottomYear() {
        return bottomYear;
    }

    public void setBottomYear(Integer bottomYear) {
        this.bottomYear = bottomYear;
    }

    public Integer getTopYear() {
        return topYear;
    }

    public void setTopYear(Integer topYear) {
        this.topYear = topYear;
    }

    public String getShipType() {
        return shipType;
    }

    public void setShipType(String shipType) {
        this.shipType = shipType;
    }

    public String getShipSizes() {
        return shipSizes;
    }

    public void setShipSizes(String shipSizes) {
        this.shipSizes = shipSizes;
    }

    public String getCaptainLastName() {
        return captainLastName;
    }

    public void setCaptainLastName(String captainLastName) {
        this.captainLastName = captainLastName;
    }

    public String getCaptainFirstNames() {
        return captainFirstNames;
    }

    public void setCaptainFirstNames(String captainFirstNames) {
        this.captainFirstNames = captainFirstNames;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getResultXml() {
        return resultXml;
    }

    public void setResultXml(String resultXml) {
        this.resultXml = resultXml;
    }

    public Integer getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(Integer numberOfResults) {
        this.numberOfResults = numberOfResults;
    }
    
}
