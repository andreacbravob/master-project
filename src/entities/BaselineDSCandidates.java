/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author andreacbravob
 */
public class BaselineDSCandidates {
    
    private Integer id;
    private Integer baselineDSId;//id from Baseline
    private String pdfUrl;//<ddd:paperurl>+":pdf"
    private String textUrl;//<dc:identifier>
    private String textTitle;//<dc:title>
    private String text;//text from textUrl
    private String textType;//<dc:type>
    private Date textDate;//<dc:date>
    private Integer collectionIssue;//<ddd:issue>
    private String collectionSpatial;//<ddd:spatial>
    private Integer collectionPage;//<ddd:page>
    private String collectionEdition;//<ddd:edition>
    private String collectionSource;//<ddd:source>
    private String collectionTitle;//<ddd:papertitle>
    private String collectionSpatialCreation;//<ddd:spatialCreation>
    private String evaluation;

    public BaselineDSCandidates() {
    }

    public BaselineDSCandidates(Integer baselineDSId, String pdfUrl, String textUrl, String textTitle, String text, String textType, Date textDate, Integer collectionIssue, String collectionSpatial, Integer collectionPage, String collectionEdition, String collectionSource, String collectionTitle, String collectionSpatialCreation) {
        this.baselineDSId = baselineDSId;
        this.pdfUrl = pdfUrl;
        this.textUrl = textUrl;
        this.textTitle = textTitle;
        this.text = text;
        this.textType = textType;
        this.textDate = textDate;
        this.collectionIssue = collectionIssue;
        this.collectionSpatial = collectionSpatial;
        this.collectionPage = collectionPage;
        this.collectionEdition = collectionEdition;
        this.collectionSource = collectionSource;
        this.collectionTitle = collectionTitle;
        this.collectionSpatialCreation = collectionSpatialCreation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBaselineDSId() {
        return baselineDSId;
    }

    public void setBaselineDSId(Integer baselineDSId) {
        this.baselineDSId = baselineDSId;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getTextUrl() {
        return textUrl;
    }

    public void setTextUrl(String textUrl) {
        this.textUrl = textUrl;
    }

    public String getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(String textTitle) {
        this.textTitle = textTitle;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextType() {
        return textType;
    }

    public void setTextType(String textType) {
        this.textType = textType;
    }

    public Date getTextDate() {
        return textDate;
    }

    public void setTextDate(Date textDate) {
        this.textDate = textDate;
    }

    public Integer getCollectionIssue() {
        return collectionIssue;
    }

    public void setCollectionIssue(Integer collectionIssue) {
        this.collectionIssue = collectionIssue;
    }

    public String getCollectionSpatial() {
        return collectionSpatial;
    }

    public void setCollectionSpatial(String collectionSpatial) {
        this.collectionSpatial = collectionSpatial;
    }

    public Integer getCollectionPage() {
        return collectionPage;
    }

    public void setCollectionPage(Integer collectionPage) {
        this.collectionPage = collectionPage;
    }

    public String getCollectionEdition() {
        return collectionEdition;
    }

    public void setCollectionEdition(String collectionEdition) {
        this.collectionEdition = collectionEdition;
    }

    public String getCollectionSource() {
        return collectionSource;
    }

    public void setCollectionSource(String collectionSource) {
        this.collectionSource = collectionSource;
    }

    public String getCollectionTitle() {
        return collectionTitle;
    }

    public void setCollectionTitle(String collectionTitle) {
        this.collectionTitle = collectionTitle;
    }

    public String getCollectionSpatialCreation() {
        return collectionSpatialCreation;
    }

    public void setCollectionSpatialCreation(String collectionSpatialCreation) {
        this.collectionSpatialCreation = collectionSpatialCreation;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }
}
