/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author andreacbravob
 */
public class Baseline {
    
    private Integer id;
    private String shipName;
    private Integer count;
    private String shipIds;
    private String shipYears;
    private String requestUrl;
    private String resultXml;
    private Integer numberOfResults;

    public Baseline() {
    }

    public Baseline(String shipName, Integer count) {
        this.shipName = shipName;
        this.count = count;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getShipIds() {
        return shipIds;
    }

    public void setShipIds(String shipIds) {
        this.shipIds = shipIds;
    }

    public String getShipYears() {
        return shipYears;
    }

    public void setShipYears(String shipYears) {
        this.shipYears = shipYears;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getResultXml() {
        return resultXml;
    }

    public void setResultXml(String resultXml) {
        this.resultXml = resultXml;
    }

    public Integer getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(Integer numberOfResults) {
        this.numberOfResults = numberOfResults;
    }
    
}
