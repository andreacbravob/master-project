/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package baseline;

import utils.HibernateUtil;
import entities.BaselineCandidates;
import entities.Baseline;
import entities.Ships;
import com.google.common.collect.LinkedListMultimap;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author andreacbravob
 */
public class ProcessBaseline {
    
    Session session = HibernateUtil.getSessionFactory().openSession();
    
    public List getDistinctShips(){
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        List<Object[]> nombresBarcos;
        String query = "SELECT DISTINCT shipName, count(shipname) FROM Ships GROUP BY shipName ORDER BY shipName";
        
        session.beginTransaction();
        nombresBarcos = session.createQuery(query).list();
        session.getTransaction().commit();

        return nombresBarcos;        
    }
    
    public List getShips(){
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        List<Ships> barcos;
        String query = "FROM Ships ORDER BY shipName";
        
        session.beginTransaction();
        barcos = session.createQuery(query).list();
        session.getTransaction().commit();

        return barcos; 
    }
    
    public List getBaseline(String query){
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        Query q;
        List<Baseline> barcos;
        
        session.beginTransaction();
        barcos = session.createQuery(query).list();

        return barcos;
    }
    
    public List getBaselineCandidates(String query){
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        Query q;
        List<BaselineCandidates> news;
        
        session.beginTransaction();
        news = session.createQuery(query).list();

        return news;
    }
    
    public List getBaselineCandidateUrl(String query, int maxResults){
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        List<Object[]> baselineCandidates;
        
        session.beginTransaction();
        baselineCandidates = session.createQuery(query).setMaxResults(maxResults).list();
        session.getTransaction().commit();

        return baselineCandidates;        
    }
    
    public List getBaselineByName(String shipName){
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        
        List<Baseline> barcos;
        Query q = session.createQuery("FROM Baseline WHERE shipName = :shipName");
        q.setString("shipName", shipName);
        
        barcos = q.list();
        session.getTransaction().commit();

        return barcos;
    }
    
    public void populateBaselineTable(List barcos){
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Iterator it = barcos.iterator();
        while (it.hasNext()) {
            Object[] nombreBarco = (Object[]) it.next();
            Baseline b = new Baseline((String) nombreBarco[0], Integer.parseInt(nombreBarco[1].toString()));
            session.save(b);
        }
        session.getTransaction().commit();
    }
            
    public void getShipIdMap(List barcos) {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        
        LinkedListMultimap<String, Integer> lmp = LinkedListMultimap.create();
                
        Iterator it = barcos.iterator();
        while (it.hasNext()) {
            Ships barco = (Ships) it.next();
            lmp.put(barco.getShipName(), (Integer) barco.getId());
        }
        
        for (String name : lmp.keySet()) {
            List<Integer> values = lmp.get(name);
            StringBuilder idString = new StringBuilder();
            for (Integer id: values) {
                idString.append(id);
                idString.append(",");
            }
            String id = idString.substring(0, idString.length()-1).toString();
            
            Query q = session.createQuery("UPDATE Baseline SET shipIds = :idP WHERE shipName = :nameP");
            q.setString("idP", id);
            q.setString("nameP", name);
            
            q.executeUpdate();
        }
        session.getTransaction().commit();
    }   
    
    public void getShipYearsMap(List barcos) {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        
        LinkedListMultimap<String, Integer> lmp = LinkedListMultimap.create();
                
        Iterator it = barcos.iterator();
        while (it.hasNext()) {
            Ships barco = (Ships) it.next();
            lmp.put(barco.getShipName(), (Integer) barco.getYear());
        }
        
        for (String name : lmp.keySet()) {
            List<Integer> values = lmp.get(name);
            StringBuilder yearString = new StringBuilder();
            for (Integer year: values) {
                yearString.append(year);
                yearString.append(",");
            }
            String years = yearString.substring(0, yearString.length()-1).toString();
            
            Query q = session.createQuery("UPDATE Baseline SET shipYears = :idP WHERE shipName = :nameP");
            q.setString("idP", years);
            q.setString("nameP", name);
            
            q.executeUpdate();
        }
        session.getTransaction().commit();
    }

    public List<Integer> getYearInterval(String shipName){
        //Get shipName
        List<Baseline> barcos = getBaselineByName(shipName);
        List<Integer> allYears = new ArrayList<>();
        List<Integer> yearInterval = new ArrayList();
        //Retrieve shipYears
        String years = barcos.get(0).getShipYears();
        //Tokenize over ,
        StringTokenizer st = new StringTokenizer(years);        
        String[] year = years.split(",");
        for (int i = 0; i < year.length; i++) {
            //Parse to Integer
            allYears.add(Integer.parseInt(year[i]));
        }
        //Order
        Collections.sort(allYears);
        //Get first, and last and save in list
        yearInterval.add(allYears.get(0));
        yearInterval.add(allYears.get(allYears.size()-1));
        //Return list
        return yearInterval;
    }
    
    public void generateUrl() {

        Session session = HibernateUtil.getSessionFactory().openSession();
        String query = "FROM Baseline ORDER BY shipName";
        //session.getSessionFactory().getCurrentSession();
        //BaseURL
        String baseURL = "http://jsru.kb.nl/sru/sru" + //JSRU is a Java implementation of the SRU protocol at the KB. This is the base URL for searching via SRU.
                "?version=1.2" + //Version, as indicated in documentation
                "&operation=searchRetrieve" + //Operation searchRetrieve, as indicated in documentation
                "&maximumRecords=100" + //Top 100 results
                "&startRecord=1" + //Start on the first result
                "&recordSchema=ddd" + // Schema DDD
                "&x-collection=DDD_artikel" + //Collection DDD
                "&query=";

        //Name of the ship
        List baselineShips = getBaseline(query);
        Iterator it = baselineShips.iterator();
        while (it.hasNext()) {
            try {
                session.beginTransaction();
                Baseline barco = (Baseline) it.next();
                String shipName = barco.getShipName();
                //Year interval from method
                List<Integer> yearInterval = getYearInterval(shipName);

                String url = "";
                url += baseURL;
                url += URLEncoder.encode("(\"" + shipName + "\")", "ISO8859_3");
                url += URLEncoder.encode(" AND date within (\"01-01-" + yearInterval.get(0) + " 31-12-" + yearInterval.get(1) + "\")", "ISO8859_3");

                //Update Baseline where name of the ship
                barco.setRequestUrl(url);
                session.saveOrUpdate(barco);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ProcessBaseline.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        session.getTransaction().commit();
    }

    public void getXmlFromKB() {

        Session session = HibernateUtil.getSessionFactory().openSession();
        String query = "FROM Baseline ORDER BY shipName";
        //URL of the shipName and yearsInterval
        List baselineShips = getBaseline(query);
        Iterator it = baselineShips.iterator();
        int counter = 0;
        try {
            while (it.hasNext()) {
                session.beginTransaction();
                Baseline barco = (Baseline) it.next();
                URL requestUrl = new URL(barco.getRequestUrl()); //get URL from database
                //read XML from KB
                BufferedReader in = new BufferedReader(new InputStreamReader(requestUrl.openStream())); 
                String line;
                StringBuilder sb = new StringBuilder();
                while ((line = in.readLine()) != null) {
                    sb.append(line);
                }
                in.close();

                barco.setResultXml(sb.toString()); //Save result XML
                session.update(barco);
                counter++;

                System.out.println(counter);
            }
            session.getTransaction().commit();
            
        } catch (MalformedURLException e) {
            //Logger.getLogger(GetXMLs.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            //Logger.getLogger(GetXMLs.class.getName()).log(Level.SEVERE, null, e);
        } catch (Exception e){
            //Logger.getLogger(GetXMLs.class.getName()).log(Level.SEVERE, null, e);
            e.getStackTrace();
        }
    }
    
    public void getNumResultsFromXml() {

        Session session = HibernateUtil.getSessionFactory().openSession();
        String query = "FROM Baseline WHERE resultXml IS NOT NULL ORDER BY shipName";

        List baselineShips = getBaseline(query);
        Iterator it = baselineShips.iterator();
        int counter = 0;
        
        try {
            System.out.println("Number of ships> " + baselineShips.size());
            while (it.hasNext()) {
                session.beginTransaction();
                Baseline barco = (Baseline) it.next();
                String xml = barco.getResultXml();
                InputStream in = new ByteArrayInputStream(xml.getBytes("UTF-8"));

                DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
                Document doc = docBuilder.parse(in);
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("srw:searchRetrieveResponse");
                Node nNode = nList.item(0);
                Element eElement = (Element) nNode;
                barco.setNumberOfResults(Integer.parseInt(eElement.getElementsByTagName("srw:numberOfRecords").item(0).getTextContent()));
                session.update(barco);

                counter++;
                System.out.println(counter);
            }
            session.getTransaction().commit();
        } catch (ParserConfigurationException | SAXException | IOException | HibernateException e) {
            e.printStackTrace();
        }
    }
    
    public void getRecords() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        List<BaselineCandidates> candidateList = new ArrayList<>();
        try {
            String query = "FROM Baseline WHERE numberOfResults>0";
            String comodin = "NOT FOUND";
            List baseline = getBaseline(query);
            Iterator it = baseline.iterator();
            
            while (it.hasNext()) {
                Baseline barco = (Baseline) it.next();
                String xml = barco.getResultXml();
                Integer baselineShipId = barco.getId();
                InputStream in = new ByteArrayInputStream(xml.getBytes("UTF-8"));

                DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
                Document doc = docBuilder.parse(in);
                doc.getDocumentElement().normalize();
                NodeList resultsList = doc.getElementsByTagName("srw:records");
                Node node = resultsList.item(0);
                NodeList records = node.getChildNodes();
                for (int i = 0; i < records.getLength(); i++) {
                    Node rec = records.item(i);
                    if (rec.getNodeType() != Node.ELEMENT_NODE) {
                        continue;
                    }
                    NodeList props = rec.getChildNodes();
                    for (int j = 0; j < props.getLength(); j++) {
                        BaselineCandidates candidate = new BaselineCandidates();
                        candidate.setBaselineShipId(baselineShipId);
                        
                        Node prop = props.item(j);
                        if (prop.getNodeType() != Node.ELEMENT_NODE) {
                            continue;
                        }
                        if (!prop.getNodeName().equals("srw:recordData")) {
                            continue;
                        }
                        //textUrl
                        NodeList todo = prop.getChildNodes();
                        String url = getNodeByName("dc:identifier", todo).getTextContent();
                        candidate.setTextUrl(url);
                        //textTitle
                        Node titulo = getNodeByName("dc:title", todo);
                        String title = comodin;
                        if (titulo != null) {
                            title = titulo.getTextContent();
                        }
                        candidate.setTextTitle(title);
                        //textType
                        Node tipo = getNodeByName("dc:type", todo);
                        String type = comodin;
                        if (tipo != null) {
                            type = tipo.getTextContent();
                        }
                        candidate.setTextType(type);
                        //texteDate
                        Date date = null;
                        try {
                            date = sdf.parse(getNodeByName("dc:date", todo).getTextContent());
                        } catch (Exception e) {
                        }
                        candidate.setTextDate(date);
                        //CollectionIssue
                        Node issue = getNodeByName("ddd:issue", todo);
                        Integer cIssue = -1;
                        if (issue != null) {
                            cIssue = Integer.parseInt(issue.getTextContent());
                        }
                        candidate.setCollectionIssue(cIssue);
                        //CollectionSpatial
                        Node spatial = getNodeByName("ddd:spatial", todo);
                        String cSpatial = comodin;
                        if (spatial != null){
                            cSpatial = spatial.getTextContent();
                        }
                        candidate.setCollectionSpatial(cSpatial);
                        //CollectionPage
                        Node page = getNodeByName("ddd:page", todo);
                        Integer cPage = -1;
                        if(page != null){
                            try{
                                cPage = Integer.parseInt(page.getTextContent());
                            }catch(Exception e){
                                cPage = -1;
                            }
                        }
                        candidate.setCollectionPage(cPage);
                        //CollectionEdition
                        Node edition = getNodeByName("ddd:edition", todo);
                        String cEdition = comodin;
                        if(edition != null){
                            cEdition = edition.getTextContent();
                        }
                        candidate.setCollectionEdition(cEdition);
                        //CollectionSource
                        Node source = getNodeByName("dc:source", todo);
                        String cSource = comodin;
                        if (source != null){
                            cSource = source.getTextContent();
                        }
                        candidate.setCollectionSource(cSource);
                        //CollectionTitle
                        Node cTitle = getNodeByName("ddd:papertitle", todo);
                        String ccTitle = comodin;
                        if(cTitle != null){
                            ccTitle = cTitle.getTextContent();
                        }
                        candidate.setCollectionTitle(ccTitle);
                        //CollectionSpatialCreation
                        Node spatialCreation = getNodeByName("ddd:spatialCreation", todo);
                        String cSpatialC = comodin;
                        if(spatialCreation != null){
                            cSpatialC = spatialCreation.getTextContent();
                        }
                        candidate.setCollectionSpatialCreation(cSpatialC);
                        
                        candidateList.add(candidate);
                    }

                }

            }
            saveToDatabase(candidateList);
        } catch (ParserConfigurationException | SAXException | IOException | HibernateException e) {
            e.printStackTrace();
        }
    }
    
    public static Node getNodeByName(String name, NodeList list) {
        Node found = null;
        for (int i = 0; i < list.getLength(); i++) {
            Node item = list.item(i);
            if (item.getNodeType() == Node.ELEMENT_NODE
                    && item.getNodeName().equals(name)) {
                found = item;
                break;
            }
        }
        return found;
    }
    
    public void saveToDatabase(List candidateList){
        Iterator it = candidateList.iterator();
        try {
            session.beginTransaction();
            while (it.hasNext()) {
                BaselineCandidates item = (BaselineCandidates) it.next();
                session.save(item);
                //System.out.println("ID: "+barquito.getId()+", Name: "+barquito.getShipName()+", year: "+barquito.getShipYear());
            }
            session.getTransaction().commit();
        }catch (JDBCException jdbce) 
        {
            jdbce.getSQLException().getNextException().printStackTrace(); 
        }
    }
    
    public void getText(){
        int max = 50000;
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        String query = "SELECT id, textUrl FROM BaselineCandidates WHERE text IS NULL";
        List<Object[]> candidates = getBaselineCandidateUrl(query, max);
        
        int size = candidates.size();
        int counter = 0;
        
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        
        session.beginTransaction();
        for(Object[] x : candidates)
        {
            Integer id = (Integer) x[0];
            String textUrl = (String) x[1];
            URL url;
            try {
                url = new URL(textUrl);
                StringBuilder sb;
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String line;
                sb = new StringBuilder();
                while ((line = in.readLine()) != null) {
                    sb.append(line);
                }

                InputStream is = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));

                DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
                Document doc = docBuilder.parse(is);
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("text");
                Node nNode = nList.item(0);
                Element eElement = (Element) nNode;
                NodeList p = eElement.getElementsByTagName("p");
                StringBuilder paragraph = new StringBuilder();

                for (int i = 0; i < p.getLength(); i++) {
                    Element pTag = (Element) p.item(i);
                    if(pTag != null){
                        paragraph.append(pTag.getTextContent());
                        paragraph.append("\n");
                    }
                }
                
                String finalP = paragraph.toString();
                String hql = "UPDATE BaselineCandidates SET text = :finalP WHERE id = :id";
                Query q = session.createQuery(hql);
                q.setString("finalP", finalP);
                q.setInteger("id", id);
                q.executeUpdate();
                
                counter++;
                System.out.println(counter+"/"+size);
                

            } catch (MalformedURLException ex) {
                Logger.getLogger(ProcessBaseline.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ProcessBaseline.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(ProcessBaseline.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(ProcessBaseline.class.getName()).log(Level.SEVERE, null, ex);
            }
  
        }
        session.getTransaction().commit();
    }
}