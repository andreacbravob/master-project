/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baseline;

import static baseline.ProcessBaselineDS.getNodeByName;
import entities.KBGeneralArticles;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.hibernate.JDBCException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import utils.HibernateUtil;

/**
 *
 * @author andreacbravob
 */
public class GetKBGeneralData {
    
    Session session = HibernateUtil.getSessionFactory().openSession();

    public String generateUrl() {

        String url = "";
        int maxResults = 200;
        Random rand = new Random();

        //BaseURL
        String baseURL = "http://jsru.kb.nl/sru/sru" + //JSRU is a Java implementation of the SRU protocol at the KB. This is the base URL for searching via SRU.
                "?version=1.2" + //Version, as indicated in documentation
                "&operation=searchRetrieve" + //Operation searchRetrieve, as indicated in documentation
                "&maximumRecords=" + maxResults +//Top 200 results
                "&startRecord=" + (rand.nextInt(maxResults) + 1) + //Start on a random result
                "&recordSchema=ddd" + // Schema DDD
                "&x-collection=DDD_artikel" + //Collection DDD
                "&query=";

        try {
            int [] years = generateRandomInts();
            url += baseURL;
            url += URLEncoder.encode("date within (\"01-01-" + years[0] + " 31-12-" + years[1] + "\")", "ISO8859_3");
            
            System.out.println(url);
        } catch (Exception e) {
            System.err.println("Encoder error");
        }
        return url;
    }

    public void getXmlAndRecords(String url) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        List<KBGeneralArticles> articleList = new ArrayList<>();

        try {
            URL requestUrl = new URL(url);
            //read XML from KB
            BufferedReader in = new BufferedReader(new InputStreamReader(requestUrl.openStream()));
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            in.close();

            //Save result XML
            String resultXML = sb.toString();
            InputStream is = new ByteArrayInputStream(resultXML.getBytes("UTF-8"));
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(is);
            doc.getDocumentElement().normalize();
            NodeList resultsList = doc.getElementsByTagName("srw:records");
            Node node = resultsList.item(0);
            NodeList records = node.getChildNodes();
            String comodin = "NOT FOUND";

            for (int i = 0; i < records.getLength(); i++) {
                Node rec = records.item(i);
                if (rec.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                NodeList props = rec.getChildNodes();
                for (int j = 0; j < props.getLength(); j++) {
                    KBGeneralArticles kbarticle = new KBGeneralArticles();

                    Node prop = props.item(j);
                    if (prop.getNodeType() != Node.ELEMENT_NODE) {
                        continue;
                    }
                    if (!prop.getNodeName().equals("srw:recordData")) {
                        continue;
                    }
                    //pdfurl
                    NodeList pdfall = prop.getChildNodes();
                    String pdfurl = getNodeByName("ddd:paperurl", pdfall).getTextContent() + ":pdf";
                    kbarticle.setPdfUrl(pdfurl);
                    //textUrl
                    NodeList todo = prop.getChildNodes();
                    String textUrl = getNodeByName("dc:identifier", todo).getTextContent();
                    kbarticle.setTextUrl(textUrl);
                    //textTitle
                    Node titulo = getNodeByName("dc:title", todo);
                    String title = comodin;
                    if (titulo != null) {
                        title = titulo.getTextContent();
                    }
                    kbarticle.setTextTitle(title);
                    //textType
                    Node tipo = getNodeByName("dc:type", todo);
                    String type = comodin;
                    if (tipo != null) {
                        type = tipo.getTextContent();
                    }
                    kbarticle.setTextType(type);
                    //texteDate
                    Date date = null;
                    try {
                        date = sdf.parse(getNodeByName("dc:date", todo).getTextContent());
                    } catch (Exception e) {
                    }
                    kbarticle.setTextDate(date);
                    //CollectionIssue
                    Node issue = getNodeByName("ddd:issue", todo);
                    Integer cIssue = -1;
                    if (issue != null) {
                        cIssue = Integer.parseInt(issue.getTextContent());
                    }
                    kbarticle.setCollectionIssue(cIssue);
                    //CollectionSpatial
                    Node spatial = getNodeByName("ddd:spatial", todo);
                    String cSpatial = comodin;
                    if (spatial != null) {
                        cSpatial = spatial.getTextContent();
                    }
                    kbarticle.setCollectionSpatial(cSpatial);
                    //CollectionPage
                    Node page = getNodeByName("ddd:page", todo);
                    Integer cPage = -1;
                    if (page != null) {
                        try {
                            cPage = Integer.parseInt(page.getTextContent());
                        } catch (Exception e) {
                            cPage = -1;
                        }
                    }
                    kbarticle.setCollectionPage(cPage);
                    //CollectionEdition
                    Node edition = getNodeByName("ddd:edition", todo);
                    String cEdition = comodin;
                    if (edition != null) {
                        cEdition = edition.getTextContent();
                    }
                    kbarticle.setCollectionEdition(cEdition);
                    //CollectionSource
                    Node source = getNodeByName("dc:source", todo);
                    String cSource = comodin;
                    if (source != null) {
                        cSource = source.getTextContent();
                    }
                    kbarticle.setCollectionSource(cSource);
                    //CollectionTitle
                    Node cTitle = getNodeByName("ddd:papertitle", todo);
                    String ccTitle = comodin;
                    if (cTitle != null) {
                        ccTitle = cTitle.getTextContent();
                    }
                    kbarticle.setCollectionTitle(ccTitle);
                    //CollectionSpatialCreation
                    Node spatialCreation = getNodeByName("ddd:spatialCreation", todo);
                    String cSpatialC = comodin;
                    if (spatialCreation != null) {
                        cSpatialC = spatialCreation.getTextContent();
                    }
                    kbarticle.setCollectionSpatialCreation(cSpatialC);

                    articleList.add(kbarticle);
                }

            }
            saveToDatabase(articleList);

        } catch (MalformedURLException e) {
            //Logger.getLogger(GetXMLs.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            //Logger.getLogger(GetXMLs.class.getName()).log(Level.SEVERE, null, e);
        } catch (Exception e) {
            //Logger.getLogger(GetXMLs.class.getName()).log(Level.SEVERE, null, e);
            e.getStackTrace();
        }
    }

    public void getText() {
        int max = 50000;
        Session session = HibernateUtil.getSessionFactory().openSession();

        String query = "SELECT id, textUrl FROM KBGeneralArticles WHERE text IS NULL";
        List<Object[]> articles = getKBArticles(query, max);

        int size = articles.size();
        int counter = 0;

        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();

        session.beginTransaction();
        for (Object[] x : articles) {
            Integer id = (Integer) x[0];
            String textUrl = (String) x[1];
            URL url;
            try {
                url = new URL(textUrl);
                StringBuilder sb;
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String line;
                sb = new StringBuilder();
                while ((line = in.readLine()) != null) {
                    sb.append(line);
                }

                InputStream is = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));

                DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
                Document doc = docBuilder.parse(is);
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("text");
                Node nNode = nList.item(0);
                Element eElement = (Element) nNode;
                NodeList p = eElement.getElementsByTagName("p");
                StringBuilder paragraph = new StringBuilder();

                for (int i = 0; i < p.getLength(); i++) {
                    Element pTag = (Element) p.item(i);
                    if (pTag != null) {
                        paragraph.append(pTag.getTextContent());
                        paragraph.append("\n");
                    }
                }

                String finalP = paragraph.toString();
                String hql = "UPDATE KBGeneralArticles SET text = :finalP WHERE id = :id";
                Query q = session.createQuery(hql);
                q.setString("finalP", finalP);
                q.setInteger("id", id);
                q.executeUpdate();

                counter++;
                System.out.println(counter + "/" + size);


            } catch (MalformedURLException ex) {
                Logger.getLogger(ProcessBaseline.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ProcessBaseline.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(ProcessBaseline.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(ProcessBaseline.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        session.getTransaction().commit();
    }
    
    public void saveToDatabase(List articleList) {

        Iterator it = articleList.iterator();
        try {
            session.beginTransaction();
            while (it.hasNext()) {
                KBGeneralArticles item = (KBGeneralArticles) it.next();
                session.save(item);
            }
            session.getTransaction().commit();
        } catch (JDBCException jdbce) {
            jdbce.getSQLException().getNextException().printStackTrace();
        }
    }
    
    public List getKBArticles(String query, int maxResults) {
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        List<Object[]> kbArticles;

        session.beginTransaction();
        kbArticles = session.createQuery(query).setMaxResults(maxResults).list();
        session.getTransaction().commit();

        return kbArticles;
    }
    
    public int[] generateRandomInts(){
        int[] years = new int[2];
        int year1 = 0;
        int year2 = 0;
        int start = 1803;//initial year of Monsterrollen collection
        int end = 1937;//final year of Monsterrollen collection

        Random rand = new Random();
        year1 = rand.nextInt(end - start) + start;
        year2 = rand.nextInt(end - start) + start;

        if (year2 > year1){
            years[0] = year1;
            years[1] = year2;
        }
        else{
            years[0] = year2;
            years[1] = year1; 
        }
        
        return years;
    }
   
}
