package baseline;

import utils.HibernateUtil;
import entities.Crew;
import entities.Ships;
import au.com.bytecode.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * In this class, there are 2 methods for importing CSV data of Ships and of Crew from the Monsterrollen collection.
 * There are also 2 hibernate methods for saving in the database
 * @author andreacbravob
 */
public class ImportData {
    
    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    Transaction tx = null;
    String fileLocation = "/Volumes/Data/Users/andreacbravob/Dropbox/VU/Year3/MasterProject/Data/Ships.csv";
    String fileLocationCrew = "/Volumes/Data/Users/andreacbravob/Dropbox/VU/Year3/MasterProject/Data/Crew.csv";
    
    public List getShipDataFromCSV() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");
        CSVReader reader;
        List barcos = new ArrayList<>();
        Ships ship = null;
        int skipped = 0;
        try {
            reader = new CSVReader(new FileReader(fileLocation), ';');
            String[] nextLine; //nextLine is the next value after the ;
            while ((nextLine = reader.readNext()) != null) {
                if (nextLine[0].toString().equals("NULL") || nextLine[2].toString().equals("NULL") || nextLine[2].toString().equals("")) {
                    skipped++;
                } else {
                    //1868-1;1868-01-22;Omega;schoener;173;Sappemeer (NL);Groningen (NL);Gloucester (GB);Groningen, Noordelijk Scheepvaartmuseum'
                    ship = new Ships();
                    ship.setmId(nextLine[0].toString());
                    try {
                        if(nextLine[1].toString().equals("NULL") || nextLine[1].toString().equals("0000-00-00"))
                            ship.setDate(year.parse(nextLine[0].toString().substring(0, 4)));
                        else
                            ship.setDate(sdf.parse(nextLine[1].toString()));
                    } catch (ParseException ex) {
                        Logger.getLogger(ImportData.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ship.setYear(Integer.parseInt(nextLine[0].toString().substring(0, 4)));
                    ship.setShipName(nextLine[2].toString());
                    ship.setShipType(nextLine[3].toString());
                    if (nextLine[4].toString().equals("NULL"))
                        ship.setShipSize(-1);
                    else
                        ship.setShipSize(Integer.parseInt(nextLine[4].toString()));
                    ship.setHomePort(nextLine[5].toString());
                    ship.setDock(nextLine[6].toString());
                    ship.setDestination(nextLine[7].toString());
                    ship.setDataSource(nextLine[8].toString());
                    barcos.add(ship);
                }
            }
            System.out.println("Skipped " + skipped);
            System.out.println("Valid ships " + barcos.size());

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImportData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex) {
            Logger.getLogger(ImportData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImportData.class.getName()).log(Level.SEVERE, null, ex);
        }

        return barcos;
    }

    public List getCrewDataFromCSV() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");
        CSVReader reader;
        List crewMembers = new ArrayList<>();
        Crew crew = null;
        int skipped = 0;
        try {
            reader = new CSVReader(new FileReader(fileLocationCrew), ';');
            String[] nextLine; //nextLine is the next value after the ;
            while ((nextLine = reader.readNext()) != null) {
                if (nextLine[0].toString().equals("NULL") || nextLine[2].toString().equals("NULL")) {
                    skipped++;
                } else {
                    //1868-1;1868-01-22;Boer, de;Jan;kapitein;NULL;Sappemeer (NL);43;Groningen, Noordelijk Scheepvaartmuseum;;;;;;;;;;
                    crew = new Crew();
                    crew.setmId(nextLine[0].toString());
                    try {
                        if(nextLine[1].toString().equals("NULL") || nextLine[1].toString().equals("0000-00-00"))
                            crew.setDate(year.parse(nextLine[0].toString().substring(0, 4)));
                        else
                            crew.setDate(sdf.parse(nextLine[1].toString()));
                    }catch (StringIndexOutOfBoundsException e){
                        e.printStackTrace();
                    } catch (ParseException ex) {
                        Logger.getLogger(ImportData.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    crew.setYear(Integer.parseInt(nextLine[0].toString().substring(0, 4)));
                    crew.setLastName(nextLine[2].toString());
                    crew.setFirstName(nextLine[3].toString());
                    crew.setRank(nextLine[4].toString());
                    if (nextLine[5].toString().equals("NULL"))
                        crew.setSalary(-1);
                    else
                        crew.setSalary(Double.parseDouble(nextLine[5].toString()));
                    crew.setResidence(nextLine[6].toString());
                    if (nextLine[7].toString().equals("NULL"))
                        crew.setAge(-1);
                    else
                        crew.setAge(Integer.parseInt(nextLine[7].toString()));
                    crew.setDatasource(nextLine[8].toString());
                    crewMembers.add(crew);
                }
            }
            System.out.println("Skipped " + skipped);
            System.out.println("Valid crew " + crewMembers.size());

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImportData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex) {
            Logger.getLogger(ImportData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImportData.class.getName()).log(Level.SEVERE, null, ex);
        }

        return crewMembers;
    }
    
    public void saveToDatabase(List barcos){
        Iterator it = barcos.iterator();

        try {
            tx = session.beginTransaction();
            while (it.hasNext()) {
                Ships barquito = (Ships) it.next();
                session.saveOrUpdate(barquito);
                System.out.println("ID: "+barquito.getId()+", mID: "+barquito.getmId());
            }
            tx.commit();
        }catch (JDBCException jdbce) 
        {
            jdbce.getSQLException().getNextException().printStackTrace(); 
        }
    }
    
    public void saveCrewToDatabase(List crewMembers){
        Iterator it = crewMembers.iterator();

        try {
            tx = session.beginTransaction();
            while (it.hasNext()) {
                Crew crews = (Crew) it.next();
                session.saveOrUpdate(crews);
                //System.out.println("ID: "+crews.getId()+", mID: "+crews.getmId());
            }
            tx.commit();
        }catch (JDBCException jdbce) 
        {
            jdbce.getSQLException().getNextException().printStackTrace(); 
        }
    }
}