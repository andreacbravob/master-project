package baseline;

import java.text.ParseException;

/**
 * Main class for preprocessing data from the Noordelijke Monsterrollen and the
 * KB newspaper archives.
 *
 * @author andreacbravob
 */
public class Main {

    public static void main(String args[]) throws InterruptedException, ParseException {
        //Uncomment to import data from the Monsterrollen on CSV format and save on database
        //importData();
        //Uncomment to preprocess baseline
        //processBaseline();
        //Uncomment to preprocess the distinct baseline
        //processBaselineDistinctShips();
        //Uncomment to get random articles from the KB and stored in the database
        //getKBGeneralData();
        //Uncomment to generate ARFF files for training
        generateTrainArffFile();
        //Uncomment to generate ARFF files for testing
//        generateTestFile(0, 100000);
//        generateTestFile(100000, 200000);
//        generateTestFile(200000, 300000);
//        generateTestFile(300000, 400000);
//        generateTestFile(400000, 500000);
    }

    /**
     * This method imports original CSV data and saves on database
     */
    public static void importData() {
        ImportData isd = new ImportData();
        isd.saveToDatabase(isd.getShipDataFromCSV());
        isd.saveCrewToDatabase(isd.getCrewDataFromCSV());
    }

    /**
     * This method preprocesses the initial data. It is used to harvest data
     * from the KB and store the results from the queries.
     *
     * @throws InterruptedException
     */
    public static void processBaseline() throws InterruptedException {
        ProcessBaseline pb = new ProcessBaseline();
        pb.populateBaselineTable(pb.getDistinctShips());
        pb.getShipIdMap(pb.getShips());
        pb.getShipYearsMap(pb.getShips());
        pb.generateUrl();
        pb.getXmlFromKB();
        pb.getNumResultsFromXml();
        pb.getRecords();
        pb.getText();
    }

    /**
     * This method is used to harvest data from the KB and store it in the
     * database
     */
    public static void processBaselineDistinctShips() {
        ProcessBaselineDS pbds = new ProcessBaselineDS();
        pbds.generateUrl();
        pbds.getXmlFromKB();
        pbds.getNumResultsFromXml();
        pbds.getRecords();
        pbds.getText();
    }

    /**
     *   This method is used to harvest random KB articles
         Note: not used in the experiments
     */
    public static void getKBGeneralData() {
        GetKBGeneralData gkb = new GetKBGeneralData();
        gkb.generateRandomInts();
        gkb.generateUrl();
        for (int i = 0; i < 10; i++) {
            gkb.getXmlAndRecords(gkb.generateUrl());
        }
        gkb.getText();
    }

    /**
     * Used to generate ARFF files for training
     */
    public static void generateTrainArffFile() {
        GenerateARFFFile gaf = new GenerateARFFFile();
        gaf.generateTrainArffFile();
    }
    
    /**
     * Used to generate files for testing
     * @param int bottom as the bottom limit for the IDs on SQL query
     * @param int top as the top limit for the IDs on SQL query
     */
    public static void generateTestFile(int bottom, int top) {
        GenerateARFFFile gaf = new GenerateARFFFile();
        gaf.generateTestFile(bottom, top);
    }
}
