package baseline;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;;
import java.util.List;
import org.hibernate.Session;
import utils.HibernateUtil;

/**
 * This class is used to generate files for both training and testing. Train sets include all labeled data, 
 * and test sets, partial unlabeled data. After, the files are merged into one manually (for test sets).
 * This is due to memory constraints.
 * The data comes from the database and the format of the resulting files is ARFF, used by WEKA for training sets
 * and text files with instances for test sets (unlabeled).
 * @author andreacbravob
 */
public class GenerateARFFFile {
    
    Session session = HibernateUtil.getSessionFactory().openSession();
    String directoryTrain = "textEvaluation_trainset.arff";
    String directoryTest = "textEvaluation_testset_";

    /**
     * Connection to the database and query the BaselineDistinctCandidates table
     * @param query
     * @param maxResults
     * @return List with results from the database
     */
    public List getBaselineDSURL(String query, int maxResults){
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        List<Object[]> baselineDS;
        
        session.beginTransaction();
        baselineDS = session.createQuery(query).setMaxResults(maxResults).list();
        session.getTransaction().commit();

        return baselineDS; 
    }
    
    /**
     * Method that generates ARFF files with labeled data for training purposes.
     */
    public void generateTrainArffFile(){
        
        int bottom = 0; 
        int top = 500000;

        String header = "@relation textEvaluation\n"
                + "\n"
                + "@attribute evaluation {1,0}\n"
                + "@attribute text String\n"
                + "\n"
                + "@data\n";
        String query = "SELECT evaluation, text FROM BaselineDSCandidates WHERE evaluation='0' or evaluation='1' AND id >"+ bottom +"AND id <= "+ top +" ORDER BY id";
        int maxResults = 200;
        BufferedWriter bw = null;
        
        String arffFile = header;

        try {
            File file = new File(directoryTrain);
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fr = null;
            BufferedWriter br = null;
            
            //Evaluation and text (attributes)
            List<Object[]> baselineDShips = getBaselineDSURL(query, maxResults);
            
            int counter = 0;
            
            StringBuffer data = new StringBuffer();
            
            for(Object[] x : baselineDShips)
            {
                String evaluation = (String) x[0];
                String text = (String) x[1];

                text = text.replaceAll("[\n\r]", "");//remove returns inside the text to avoid incomplete texts
                text = text.replaceAll("\"", "");// remove "" from inside the text to avoid problems in reading texts
                
                data.append(evaluation);
                data.append(",");
                data.append("\"");
                data.append(text);
                data.append("\"");
                data.append(System.getProperty("line.separator"));
                
                counter++;
            }
            System.out.println(counter);
            //Save file
            try {
                fr = new FileWriter(file);
                br = new BufferedWriter(fr);
                br.write(arffFile+data);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close();
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Method that generates text files with unlabeled data for testing purposes.
     * The parameters are used given memory constraints.
     * @param bottom
     * @param top 
     */
    public void generateTestFile(int bottom, int top){
        
        String header = "";
        String query = "SELECT evaluation, text FROM BaselineDSCandidates WHERE evaluation='?' AND id >"+ bottom +"AND id <= "+ top +" ORDER by id";
        
        int maxResults = 100000;
        BufferedWriter bw = null;
        
        String arffFile = header;

        try {
            File file = new File(directoryTest+bottom+".txt");
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fr = null;
            BufferedWriter br = null;
            
            //Evaluation and text
            List<Object[]> baselineDShips = getBaselineDSURL(query, maxResults);
            
            int counter = 0;
            
            StringBuffer data = new StringBuffer();
            
            for(Object[] x : baselineDShips)
            {
                String text = (String) x[1];

                text = text.replaceAll("[\n\r]", "");//remove returns inside the text to avoid incomplete texts
                text = text.replaceAll("\"", "");// remove "" from inside the text to avoid problems in reading texts
                
                data.append(text);
                data.append(System.getProperty("line.separator"));
                
                counter++;
            }
            System.out.println(counter);
            //Save file
            try {
                fr = new FileWriter(file);
                br = new BufferedWriter(fr);
                br.write(arffFile+data);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close();
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
