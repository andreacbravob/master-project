package classifier;

import entities.NaiveBayesLD;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;
import utils.HibernateUtil;

/**
 * This class is used to import text files that the classifier model has output.
 * Then, it stores it in the database
 *
 * @author andreacbravob
 */
public class ImportNaiveBayesOutput {

    Session session = HibernateUtil.getSessionFactory().openSession();

    public void populateLabeledData(List data) {
        session.beginTransaction();

        Iterator it = data.iterator();
        while (it.hasNext()) {
            Object[] instance = (Object[]) it.next();
            NaiveBayesLD item = new NaiveBayesLD(Integer.parseInt(instance[0].toString()), (String) instance[1]);
            session.save(item);
        }
        session.getTransaction().commit();
    }

    public List loadData(String fileName) {
        List instances = new ArrayList();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("@") || line.equals("")) {
                    //System.out.println("Line Skipped");
                } else {
                    Object[] instance = new Object[2];
                    instance[0] = line.substring(0, 1);//label
                    instance[1] = line.substring(line.indexOf('\'') + 1, line.length() - 1);//text
                    instances.add(instance);
                }
            }
            reader.close();
        } catch (IOException e) {
            System.out.println("Problem found when reading: " + fileName);
            e.printStackTrace();
        }
        return instances;
    }

    public static void main(String args[]) {
        ImportNaiveBayesOutput importer = new ImportNaiveBayesOutput();

        int offset = 100000;
        int newNum = 0;
        for (int i = 0; i < 5; i++) {
            //Partial name of the testSet
            String newFile = "naivebayes/newLabeledData_";

            if (i == 0) {
                newNum = i;
            } else {
                newNum = i * offset;
            }
            newFile = newFile + newNum + ".txt";
            System.out.println(">>>> Beginning of file: "+newFile);
            importer.populateLabeledData(importer.loadData(newFile));
            System.out.println(">>>> End of file: "+newFile);
        }
    }
}
