package classifier;

/**
 * A Java class that implements a text classifier, based on WEKA. WEKA is
 * available at:http://www.cs.waikato.ac.nz/ml/weka/ Largely based on the code
 * by Jose Maria Gomez Hidalgo - http://www.esp.uem.es/jmgomez To be used with
 * TrainingClassifier.java
 */
import weka.core.*;
import weka.core.FastVector;
import weka.classifiers.meta.FilteredClassifier;
import java.util.List;
import java.util.ArrayList;
import java.io.*;

/**
 * This class implements a text classifier in Java using WEKA. It loads a file
 * with the text to classify, and the model that has been learned with
 * TrainingClassifier.java.
 *
 * @author Andrea Bravo Balado
 * @author Jose Maria Gomez Hidalgo - http://www.esp.uem.es/jmgomez
 * @see TrainingClassifier.java
 */
public class TestingModel {

    /**
     * Object that stores the instances.
     */
    Instances instances;
    /**
     * Object that stores the classifier.
     */
    FilteredClassifier classifier;
    /**
     * Object that stores each instance in an array
     */
    List<String> instancesFromTest = new ArrayList<>();

    /**
     * This method loads the text to be classified and stores it in an ArrayList
     *
     * @param fileName The name of the file that stores the text.
     */
    public void load(String fileName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line;
            while ((line = reader.readLine()) != null) {
                instancesFromTest.add(line);
            }
            reader.close();
        } catch (IOException e) {
            System.out.println("Problem found when reading: " + fileName);
            e.printStackTrace();
        }
    }

    /**
     * This method loads the model to be used as classifier. (The model was
     * previously generated, evaluated and trained using the
     * TrainingClassifier.java class
     *
     * @param fileName The name of the file that stores the text.
     */
    public void loadModel(String fileName) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
            Object tmp = in.readObject();
            classifier = (FilteredClassifier) tmp;
            in.close();
        } catch (Exception e) {
            // Given the cast, a ClassNotFoundException must be caught along with the IOException
            System.out.println("Problem found when reading: " + fileName);
            e.printStackTrace();
        }
    }

    /**
     * This method creates the instances to be classified, from the text that
     * has been read, until the text is over.
     */
    public void makeInstance() {
        // Create the attributes, class and text
        FastVector fvNominalVal = new FastVector(2);
        fvNominalVal.addElement("1");//positive 
        fvNominalVal.addElement("0");//negative
        Attribute attribute1 = new Attribute("class", fvNominalVal);
        Attribute attribute2 = new Attribute("text", (FastVector) null);
        // Create list of instances with one element (the other is unknown)
        FastVector fvWekaAttributes = new FastVector(2);
        fvWekaAttributes.addElement(attribute1);
        fvWekaAttributes.addElement(attribute2);
        instances = new Instances("textEvaluation", fvWekaAttributes, 1);
        // Set class index
        instances.setClassIndex(0); //Attribute 0 is the class (evaluation)
        for (String x : instancesFromTest) {
            // Create and add the instance
            DenseInstance instance = new DenseInstance(2);
            instance.setValue(attribute2, x);
            instances.add(instance);
        }
    }

    /**
     * This method performs the classification of the instances. Output, which
     * is newly labeled data, is stored on a file
     *
     * @param newFile Name of the file
     */
    public void classify(String newFile) {
        StringBuffer newLabeledData = new StringBuffer();
        for (int i = 0; i < instances.size(); i++) {
            try {
                double pred = classifier.classifyInstance(instances.instance(i));
                instances.instance(i).setClassValue(pred);
                //Uncomment to see predictions
                //System.out.println(instances.instance(i));

            } catch (Exception e) {
                System.out.println("Problem found when classifying the text");
                e.printStackTrace();
            }
        }
        newLabeledData.append(instances);
        saveLabels(newFile, newLabeledData);
    }

    /**
     * This method saves text into file
     *
     * @param fileName
     * @param newLabeledData
     */
    public void saveLabels(String fileName, StringBuffer newLabeledData) {
        FileWriter fr = null;
        BufferedWriter br = null;
        try {
            fr = new FileWriter(fileName);
            br = new BufferedWriter(fr);
            br.write(newLabeledData.toString());
            System.out.println(">>> File: " + fileName + " saved");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Main method.
     *
     * @param args
     */
    public static void main(String[] args) {

        //Uncomment to run testing for a model trained with NaiveBayes
        String folder = "naivebayes/";
        //Uncomment to run testing for a model trained with SMO
        //String folder = "smo/";
        //Naming the test files "dynamically"
        int offset = 100000;
        int newNum = 0;
        //Name of the model
        String model = folder+"textEvaluation_model.dat";
        for (int i = 0; i < 5; i++) {
            //Partial name of the testSet
            String testSet = folder+"textEvaluation_testset_";
            String newFile = folder+"newLabeledData_";

            if (i == 0) {
                newNum = i;
            } else {
                newNum = i * offset;
            }

            testSet = testSet + newNum + ".txt";
            newFile = newFile + newNum + ".txt";

            System.out.println(">>>> Beginning of file: " + testSet);

            //Testing model
             TestingModel classifier;
             classifier = new TestingModel();
             //Load test set
             classifier.load(testSet);
             //Load model
             classifier.loadModel(model);
             //Make instances
             classifier.makeInstance();
             //Classify and save data
             classifier.classify(newFile);
            System.out.println(">>>> End of file: " + testSet);
        }
    }
}
