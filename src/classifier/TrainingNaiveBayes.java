package classifier;

/**
 * A Java class that implements a text learner, based on WEKA. WEKA is available
 * at: http://www.cs.waikato.ac.nz/ml/weka/ Largely based on the code by Jose
 * Maria Gomez Hidalgo - http://www.esp.uem.es/jmgomez To be used with
 * TestingModel.java
 */
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;
import weka.classifiers.Evaluation;
import java.util.Random;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.converters.ArffLoader.ArffReader;
import java.io.*;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.core.tokenizers.AlphabeticTokenizer;
import weka.filters.MultiFilter;
import weka.filters.unsupervised.attribute.NumericToNominal;
import weka.filters.unsupervised.attribute.RemoveByName;

/**
 * This class implements a simple learner in Java using WEKA. It loads a text
 * dataset written in ARFF format, evaluates a classifier on it, and saves the
 * learned model for further use.
 *
 * @author Andrea Bravo Balado
 * @author Jose Maria Gomez Hidalgo - http://www.esp.uem.es/jmgomez
 * @see TestingModel.java
 */
public class TrainingNaiveBayes {

    /**
     * Object that stores training data.
     */
    Instances trainData;
    /**
     * Object that stores the filters: MultiFilter, StringToWordVector,
     * RemoveByName, NumericToNominal and AttributeSelection
     */
    MultiFilter multi;
    StringToWordVector stringToWordVector;
    RemoveByName removeShortWords;
    NumericToNominal numericToNominal;
    AttributeSelection attributeSelection;
    /**
     * Object that stores the classifier
     */
    FilteredClassifier classifier;

    /**
     * This method loads a dataset in ARFF format. If the file does not exist,
     * or it has a wrong format, the attribute trainData is null.
     *
     * @param fileName The name of the file that stores the dataset.
     */
    public void loadDataset(String fileName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            ArffReader arff = new ArffReader(reader);
            trainData = arff.getData();
            System.out.println(">>>> Loaded dataset: " + fileName);
            reader.close();
        } catch (IOException e) {
            System.out.println("Problem found when reading: " + fileName);
            e.printStackTrace();
        }
    }

    /**
     * This method evaluates the classifier. As recommended by WEKA
     * documentation, the classifier is defined but not trained yet. Evaluation
     * of previously trained classifiers can lead to unexpected results.
     */
    public void evaluate() {
        try {
            //First attribute is the class (evaluation)
            trainData.setClassIndex(0);
            //Multifilter, so that several filters can be used
            multi = new MultiFilter();
            //StringToWordVector and its configuration
            stringToWordVector = new StringToWordVector();
            //IDFTransform: true
            stringToWordVector.setIDFTransform(true);
            //TFTranform: true
            stringToWordVector.setTFTransform(true);
            //AttributeIndices: last
            stringToWordVector.setAttributeIndices("last");
            //lowerCaseTokens: true
            stringToWordVector.setLowerCaseTokens(true);
            //minTermFrequency: 2
            stringToWordVector.setMinTermFreq(2);
            //Stopwords
            stringToWordVector.setStopwords(new File("stopwords"));
            stringToWordVector.setUseStoplist(true);
            //Tokenizer
            stringToWordVector.setTokenizer(new AlphabeticTokenizer());
            //Words to keep
            stringToWordVector.setWordsToKeep(300);
            //RemoveByName and its configuration
            removeShortWords = new RemoveByName();
            removeShortWords.setExpression("\\b\\w{1,3}\\b");
            //Transform in order to apply the next filter
            numericToNominal = new NumericToNominal();
            numericToNominal.setAttributeIndices("2-last");
            //Attribute selection with Information Gain and Ranker
            attributeSelection = new AttributeSelection();
            InfoGainAttributeEval infoGainEval = new InfoGainAttributeEval();
            Ranker search = new Ranker();
            attributeSelection.setEvaluator(infoGainEval);
            attributeSelection.setSearch(search);

            //Group all filters
            Filter[] groupedFilters = new Filter[]{stringToWordVector, removeShortWords, numericToNominal, attributeSelection};
            multi.setFilters(groupedFilters);
            //Classifier configuration
            classifier = new FilteredClassifier();
            classifier.setFilter(multi);
            classifier.setClassifier(new NaiveBayes());
            //Evaluation
            Evaluation eval = new Evaluation(trainData);
            //10-fold cross validation
            eval.crossValidateModel(classifier, trainData, 10, new Random(1));
            //Print statistics
            saveLog("naivebayes/trainLog_eval.txt", eval.toSummaryString() + "\n" + eval.toClassDetailsString() + "\n" +eval.toMatrixString());
            System.out.println(eval.toSummaryString());
            System.out.println(eval.toClassDetailsString());
            System.out.println(">>>> Evaluating on filtered dataset: done");
        } catch (Exception e) {
            System.out.println("Problem found when evaluating");
            e.printStackTrace();
        }
    }

    /**
     * This method trains the classifier on the loaded dataset.
     */
    public void learn() {
        try {
            //First attribute is the class (evaluation)
            trainData.setClassIndex(0);
            //Multifilter, so that several filters can be used
            multi = new MultiFilter();
            //StringToWordVector and its configuration
            stringToWordVector = new StringToWordVector();
            //IDFTransform: true
            stringToWordVector.setIDFTransform(true);
            //TFTranform: true
            stringToWordVector.setTFTransform(true);
            //AttributeIndices: last
            stringToWordVector.setAttributeIndices("last");
            //lowerCaseTokens: true
            stringToWordVector.setLowerCaseTokens(true);
            //minTermFrequency: 2
            stringToWordVector.setMinTermFreq(2);
            //Stopwords
            stringToWordVector.setStopwords(new File("stopwords"));
            stringToWordVector.setUseStoplist(true);
            //Tokenizer
            stringToWordVector.setTokenizer(new AlphabeticTokenizer());
            //Words to keep
            stringToWordVector.setWordsToKeep(300);
            //RemoveByName and its configuration
            removeShortWords = new RemoveByName();
            removeShortWords.setExpression("\\b\\w{1,3}\\b");
            //Transform in order to apply the next filter
            numericToNominal = new NumericToNominal();
            numericToNominal.setAttributeIndices("2-last");
            //Attribute selection with Information Gain and Ranker
            attributeSelection = new AttributeSelection();
            InfoGainAttributeEval infoGainEval = new InfoGainAttributeEval();
            Ranker search = new Ranker();
            attributeSelection.setEvaluator(infoGainEval);
            attributeSelection.setSearch(search);

            //Group all filters
            Filter[] groupedFilters = new Filter[]{stringToWordVector, removeShortWords, numericToNominal, attributeSelection};
            multi.setFilters(groupedFilters);
            //Classifier configuration
            classifier = new FilteredClassifier();
            classifier.setFilter(multi);
            classifier.setClassifier(new NaiveBayes());
            //Build the classifier
            classifier.buildClassifier(trainData);
            //Print statistics
            saveLog("naivebayes/trainLog_learn.txt", classifier.toString());
            System.out.println(classifier);
            System.out.println(">>>> Training on filtered dataset: done");
        } catch (Exception e) {
            System.out.println("Problem found when training");
            e.printStackTrace();
        }
    }

    /**
     * This method saves the trained model into a file. This is done by simple
     * serialization of the classifier object.
     *
     * @param fileName The name of the file that will store the trained model.
     */
    public void saveModel(String fileName) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
            out.writeObject(classifier);
            out.close();
            System.out.println(">>>> Saved model: " + fileName);
        } catch (IOException e) {
            System.out.println("Problem found when writing: " + fileName);
            e.printStackTrace();
        }
    }

    /**
     * This method saves a log
     *
     * @param fileName name of the file
     * @param log the text to be logged
     */
    public void saveLog(String fileName, String log) {
        FileWriter fr = null;
        BufferedWriter br = null;
        try {
            fr = new FileWriter(fileName);
            br = new BufferedWriter(fr);
            br.write(log.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Main method.
     * @param args[]
     */
    public static void main(String[] args) {

        //Training data in ARFF
        String dataset = "naivebayes/textEvaluation_trainset.arff";
        //The name of the model to be saved
        String model = "naivebayes/textEvaluation_model.dat";

        TrainingNaiveBayes learner;
        learner = new TrainingNaiveBayes();
        //Loading dataset
        learner.loadDataset(dataset);
        // Evaluation must be done before training. More info in: http://weka.wikispaces.com/Use+WEKA+in+your+Java+code
        learner.evaluate();
        //Learning
        learner.learn();
        //Saving model
        learner.saveModel(model);
    }
}
